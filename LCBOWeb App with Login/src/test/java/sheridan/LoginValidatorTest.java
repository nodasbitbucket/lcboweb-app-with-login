package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// 4 tests for the first requirement
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ShinnosukeNoda" ) );
	}
	
	@Test
	public void testIsValidLoginException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Shin1234" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Shin1234@" ) );
	}
	
	// 4 tests for the second requirement
	@Test
	public void testIsValidLoginNameLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "12345Shin" ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Shin12" ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Shin1" ) );
	}
	

}
