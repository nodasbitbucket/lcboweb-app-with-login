package sheridan;

public class LoginValidator {

	// Require 1: Login name must have characters and numbers only
	// Require 2: Login name must have at least 6 characters or numbers.
	public static boolean isValidLoginName( String loginName ) {
		int countCharOrNum = 0;
		
		// Check Requirement 1
		for (int i = 0; i < loginName.length(); i++) {
			// Is it a character or a number
			if (!(Character.isLetter(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))) {
				return false;
			} else {
				countCharOrNum++;
			}
		}
		
		
		// Check Requirement 2
		// Dose it have at least 6 characters or numbers?
		if (countCharOrNum < 6) {
			return false;
		}
		
		// It passed all requirements
		return true;
	}
}
